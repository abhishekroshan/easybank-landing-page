const menuButton = document.getElementById("hamburger");
const menuItems = document.getElementById("menu");

menuButton.addEventListener("click", function () {
  if (menuItems.style.display === "block") {
    menuItems.style.display = "none";
    menuButton.innerHTML = `<img src="./images//icon-hamburger.svg">`;
  } else {
    menuItems.style.display = "block";
    menuButton.innerHTML = `<img src="./images//icon-close.svg">`;
  }
});
